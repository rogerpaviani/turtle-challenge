﻿using Challenge.Settings;
using System;
using Xunit;

namespace Challenge.Tests.Unit
{
    public class GameUnitTests
    {
        [Fact]
        public void WhenNullSettings1_ThenExceptionIsThrown()
        {
            //arrange
            GameSettings settings = null;

            //act+assert
            Assert.Throws<ArgumentNullException>(() =>
            {
                var game = new Game(settings);
            });
        }

        [Fact]
        public void WhenNullSettings2_ThenExceptionIsThrown()
        {
            //arrange
            GameSettings settings = new GameSettings
            {
                BoardSize = null,
                Direction = Direction.East,
                ExitPoint = new Tuple<ushort, ushort>(0, 1),
                Mines = new Tuple<ushort, ushort>[] { },
                StartPoint = new Tuple<ushort, ushort>(5, 0)
            };

            //act+assert
            Assert.Throws<ArgumentNullException>(() =>
            {
                var game = new Game(settings);
            });
        }

        [Fact]
        public void WhenNullSettings3_ThenExceptionIsThrown()
        {
            //arrange
            GameSettings settings = new GameSettings
            {
                BoardSize = new Tuple<ushort, ushort>(2, 2),
                Direction = Direction.East,
                ExitPoint = null,
                Mines = new Tuple<ushort, ushort>[] { },
                StartPoint = new Tuple<ushort, ushort>(5, 0)
            };

            //act+assert
            Assert.Throws<ArgumentNullException>(() =>
            {
                var game = new Game(settings);
            });
        }

        [Fact]
        public void WhenNullSettings4_ThenExceptionIsThrown()
        {
            //arrange
            GameSettings settings = new GameSettings
            {
                BoardSize = new Tuple<ushort, ushort>(2, 2),
                Direction = Direction.East,
                ExitPoint = new Tuple<ushort, ushort>(0, 1),
                Mines = null,
                StartPoint = new Tuple<ushort, ushort>(5, 0)
            };

            //act+assert
            Assert.Throws<ArgumentNullException>(() =>
            {
                var game = new Game(settings);
            });
        }

        [Fact]
        public void WhenNullSettings5_ThenExceptionIsThrown()
        {
            //arrange
            GameSettings settings = new GameSettings
            {
                BoardSize = new Tuple<ushort, ushort>(2, 2),
                Direction = Direction.East,
                ExitPoint = new Tuple<ushort, ushort>(0, 1),
                Mines = new Tuple<ushort, ushort>[] { },
                StartPoint = null
            };

            //act+assert
            Assert.Throws<ArgumentNullException>(() =>
            {
                var game = new Game(settings);
            });
        }

        [Fact]
        public void WhenInvalidStartPoint_ThenExceptionIsThrown()
        {
            //arrange
            GameSettings settings = new GameSettings
            {
                BoardSize = new Tuple<ushort, ushort>(2, 2),
                Direction = Direction.West,
                ExitPoint = new Tuple<ushort, ushort>(0, 1),
                Mines = new Tuple<ushort, ushort>[] { },
                StartPoint = new Tuple<ushort, ushort>(5, 0)
            };

            //act+assert
            Assert.Throws<ArgumentOutOfRangeException>(() =>
            {
                var game = new Game(settings);
            });
        }

        [Fact]
        public void WhenInvalidExitPoint_ThenExceptionIsThrown()
        {
            //arrange
            GameSettings settings = new GameSettings
            {
                BoardSize = new Tuple<ushort, ushort>(2, 2),
                Direction = Direction.North,
                ExitPoint = new Tuple<ushort, ushort>(0, 0),
                Mines = new Tuple<ushort, ushort>[] { },
                StartPoint = new Tuple<ushort, ushort>(0, 0)
            };

            //act+assert
            Assert.Throws<ArgumentException>(() =>
            {
                var game = new Game(settings);
            });
        }

        [Fact]
        public void WhenInvalidMinePosition_ThenExceptionIsThrown()
        {
            //arrange
            GameSettings settings = new GameSettings
            {
                BoardSize = new Tuple<ushort, ushort>(2, 2),
                Direction = Direction.South,
                ExitPoint = new Tuple<ushort, ushort>(0, 1),
                Mines = new[] { new Tuple<ushort, ushort>(5, 0) },
                StartPoint = new Tuple<ushort, ushort>(0, 0)
            };

            //act+assert
            Assert.Throws<ArgumentOutOfRangeException>(() =>
            {
                var game = new Game(settings);
            });
        }

        [Fact]
        public void WhenStartpointAndExitpointEquals_ThenExceptionIsThrown()
        {
            //arrange
            GameSettings settings = new GameSettings
            {
                BoardSize = new Tuple<ushort, ushort>(2, 2),
                Direction = Direction.East,
                ExitPoint = new Tuple<ushort, ushort>(0, 10),
                Mines = new Tuple<ushort, ushort>[] { },
                StartPoint = new Tuple<ushort, ushort>(0, 0)
            };

            //act+assert
            Assert.Throws<ArgumentOutOfRangeException>(() =>
            {
                var game = new Game(settings);
            });
        }

        [Fact]
        public void WhenStartpointAndMinePositionEquals_ThenExceptionIsThrown()
        {
            //arrange
            GameSettings settings = new GameSettings
            {
                BoardSize = new Tuple<ushort, ushort>(2, 2),
                Direction = Direction.East,
                ExitPoint = new Tuple<ushort, ushort>(0, 1),
                Mines = new[] { new Tuple<ushort, ushort>(0, 0) },
                StartPoint = new Tuple<ushort, ushort>(0, 0)
            };

            //act+assert
            Assert.Throws<ArgumentException>(() =>
            {
                var game = new Game(settings);
            });
        }

        [Fact]
        public void WhenExitpointAndMinePositionEquals_ThenExceptionIsThrown()
        {
            //arrange
            GameSettings settings = new GameSettings
            {
                BoardSize = new Tuple<ushort, ushort>(2, 2),
                Direction = Direction.East,
                ExitPoint = new Tuple<ushort, ushort>(0, 1),
                Mines = new[] { new Tuple<ushort, ushort>(0, 1) },
                StartPoint = new Tuple<ushort, ushort>(0, 0)
            };

            //act+assert
            Assert.Throws<ArgumentException>(() =>
            {
                var game = new Game(settings);
            });
        }

        [Fact]
        public void WhenReset_ThenMoveBackToStartPoint()
        {
            //arrange
            GameSettings settings = new GameSettings
            {
                BoardSize = new Tuple<ushort, ushort>(2, 2),
                Direction = Direction.East,
                ExitPoint = new Tuple<ushort, ushort>(0, 1),
                Mines = new Tuple<ushort, ushort>[] { },
                StartPoint = new Tuple<ushort, ushort>(0, 0)
            };

            var game = new Game(settings);

            string moves = "MRM";

            //act
            game.Play(moves);
            game.Reset();

            //assert
            Assert.True(new Tuple<ushort, ushort>(0, 0).Equals(game.CurrentPosition));
        }

        [Fact]
        public void WhenPlay1_ThenStillInDanger()
        {
            //arrange
            int mineHit = 0;
            int stillInDanger = 0;
            int success = 0;

            GameSettings settings = new GameSettings
            {
                BoardSize = new Tuple<ushort, ushort>(2, 2),
                Direction = Direction.South,
                ExitPoint = new Tuple<ushort, ushort>(0, 1),
                Mines = new Tuple<ushort, ushort>[] { },
                StartPoint = new Tuple<ushort, ushort>(0, 0)
            };

            var game = new Game(settings);
            game.OnMineHit += (sender, e) => { mineHit++; };
            game.OnStillInDanger += (sender, e) => { stillInDanger++; };
            game.OnSuccess += (sender, e) => { success++; };

            string moves = "RRRRRRR";

            //act
            game.Play(moves);

            //assert
            Assert.Equal(0, mineHit);
            Assert.Equal(1, stillInDanger);
            Assert.Equal(0, success);
            Assert.True(new Tuple<ushort, ushort>(0, 0).Equals(game.CurrentPosition));
        }

        [Fact]
        public void WhenPlay2_ThenStillInDanger()
        {
            //arrange
            int mineHit = 0;
            int stillInDanger = 0;
            int success = 0;

            GameSettings settings = new GameSettings
            {
                BoardSize = new Tuple<ushort, ushort>(2, 2),
                Direction = Direction.South,
                ExitPoint = new Tuple<ushort, ushort>(0, 1),
                Mines = new Tuple<ushort, ushort>[] { },
                StartPoint = new Tuple<ushort, ushort>(0, 0)
            };

            var game = new Game(settings);
            game.OnMineHit += (sender, e) => { mineHit++; };
            game.OnStillInDanger += (sender, e) => { stillInDanger++; };
            game.OnSuccess += (sender, e) => { success++; };

            string moves = "RRRMRMR";

            //act
            game.Play(moves);

            //assert
            Assert.Equal(0, mineHit);
            Assert.Equal(1, stillInDanger);
            Assert.Equal(0, success);
            Assert.True(new Tuple<ushort, ushort>(1, 1).Equals(game.CurrentPosition));
        }

        [Fact]
        public void WhenPlay3_ThenStillInDanger()
        {
            //arrange
            int mineHit = 0;
            int stillInDanger = 0;
            int success = 0;

            GameSettings settings = new GameSettings
            {
                BoardSize = new Tuple<ushort, ushort>(2, 2),
                Direction = Direction.South,
                ExitPoint = new Tuple<ushort, ushort>(0, 1),
                Mines = new Tuple<ushort, ushort>[] { },
                StartPoint = new Tuple<ushort, ushort>(0, 0)
            };

            var game = new Game(settings);
            game.OnMineHit += (sender, e) => { mineHit++; };
            game.OnStillInDanger += (sender, e) => { stillInDanger++; };
            game.OnSuccess += (sender, e) => { success++; };

            string moves = "RRRMRMRRMRRRMRRRRRRABCDEFG";

            //act
            game.Play(moves);

            //assert
            Assert.Equal(0, mineHit);
            Assert.Equal(1, stillInDanger);
            Assert.Equal(0, success);
            Assert.True(new Tuple<ushort, ushort>(0, 0).Equals(game.CurrentPosition));
        }

        [Fact]
        public void WhenPlay4_ThenSuccess()
        {
            //arrange
            int mineHit = 0;
            int stillInDanger = 0;
            int success = 0;

            GameSettings settings = new GameSettings
            {
                BoardSize = new Tuple<ushort, ushort>(2, 2),
                Direction = Direction.South,
                ExitPoint = new Tuple<ushort, ushort>(0, 1),
                Mines = new Tuple<ushort, ushort>[] { },
                StartPoint = new Tuple<ushort, ushort>(0, 0)
            };

            var game = new Game(settings);
            game.OnMineHit += (sender, e) => { mineHit++; };
            game.OnStillInDanger += (sender, e) => { stillInDanger++; };
            game.OnSuccess += (sender, e) => { success++; };

            string moves = "M";

            //act
            game.Play(moves);

            //assert
            Assert.Equal(0, mineHit);
            Assert.Equal(0, stillInDanger);
            Assert.Equal(1, success);
            Assert.True(new Tuple<ushort, ushort>(0, 1).Equals(game.CurrentPosition));
        }

        [Fact]
        public void WhenPlay5_ThenSuccess()
        {
            //arrange
            int mineHit = 0;
            int stillInDanger = 0;
            int success = 0;

            GameSettings settings = new GameSettings
            {
                BoardSize = new Tuple<ushort, ushort>(2, 2),
                Direction = Direction.South,
                ExitPoint = new Tuple<ushort, ushort>(0, 1),
                Mines = new Tuple<ushort, ushort>[] { },
                StartPoint = new Tuple<ushort, ushort>(0, 0)
            };

            var game = new Game(settings);
            game.OnMineHit += (sender, e) => { mineHit++; };
            game.OnStillInDanger += (sender, e) => { stillInDanger++; };
            game.OnSuccess += (sender, e) => { success++; };

            string moves = "RRRRM";

            //act
            game.Play(moves);

            //assert
            Assert.Equal(0, mineHit);
            Assert.Equal(0, stillInDanger);
            Assert.Equal(1, success);
            Assert.True(new Tuple<ushort, ushort>(0, 1).Equals(game.CurrentPosition));
        }

        [Fact]
        public void WhenPlay6_ThenSuccess()
        {
            //arrange
            int mineHit = 0;
            int stillInDanger = 0;
            int success = 0;

            GameSettings settings = new GameSettings
            {
                BoardSize = new Tuple<ushort, ushort>(2, 2),
                Direction = Direction.South,
                ExitPoint = new Tuple<ushort, ushort>(0, 1),
                Mines = new[] { new Tuple<ushort, ushort>(1, 1) },
                StartPoint = new Tuple<ushort, ushort>(0, 0)
            };

            var game = new Game(settings);
            game.OnMineHit += (sender, e) => { mineHit++; };
            game.OnStillInDanger += (sender, e) => { stillInDanger++; };
            game.OnSuccess += (sender, e) => { success++; };

            string moves = "RRRRM";

            //act
            game.Play(moves);

            //assert
            Assert.Equal(0, mineHit);
            Assert.Equal(0, stillInDanger);
            Assert.Equal(1, success);
            Assert.True(new Tuple<ushort, ushort>(0, 1).Equals(game.CurrentPosition));
        }

        [Fact]
        public void WhenPlay7_ThenSuccess()
        {
            //arrange
            int mineHit = 0;
            int stillInDanger = 0;
            int success = 0;

            GameSettings settings = new GameSettings
            {
                BoardSize = new Tuple<ushort, ushort>(3, 3),
                Direction = Direction.North,
                ExitPoint = new Tuple<ushort, ushort>(2, 0),
                Mines = new[] { new Tuple<ushort, ushort>(1, 0), new Tuple<ushort, ushort>(1, 1) },
                StartPoint = new Tuple<ushort, ushort>(0, 0)
            };

            var game = new Game(settings);
            game.OnMineHit += (sender, e) => { mineHit++; };
            game.OnStillInDanger += (sender, e) => { stillInDanger++; };
            game.OnSuccess += (sender, e) => { success++; };

            string moves = "RRMMRRRMMRRRMM";

            //act
            game.Play(moves);

            //assert
            Assert.Equal(0, mineHit);
            Assert.Equal(0, stillInDanger);
            Assert.Equal(1, success);
            Assert.True(new Tuple<ushort, ushort>(2, 0).Equals(game.CurrentPosition));
        }

        [Fact]
        public void WhenPlay8_ThenMineHit()
        {
            //arrange
            int mineHit = 0;
            int stillInDanger = 0;
            int success = 0;

            GameSettings settings = new GameSettings
            {
                BoardSize = new Tuple<ushort, ushort>(2, 2),
                Direction = Direction.East,
                ExitPoint = new Tuple<ushort, ushort>(1, 0),
                Mines = new[] { new Tuple<ushort, ushort>(0, 1) },
                StartPoint = new Tuple<ushort, ushort>(0, 0)
            };

            var game = new Game(settings);
            game.OnMineHit += (sender, e) => { mineHit++; };
            game.OnStillInDanger += (sender, e) => { stillInDanger++; };
            game.OnSuccess += (sender, e) => { success++; };

            string moves = "RM";

            //act
            game.Play(moves);

            //assert
            Assert.Equal(1, mineHit);
            Assert.Equal(0, stillInDanger);
            Assert.Equal(0, success);
            Assert.True(new Tuple<ushort, ushort>(0, 1).Equals(game.CurrentPosition));
        }

        [Fact]
        public void WhenPlay9_ThenMineHit()
        {
            //arrange
            int mineHit = 0;
            int stillInDanger = 0;
            int success = 0;

            GameSettings settings = new GameSettings
            {
                BoardSize = new Tuple<ushort, ushort>(3, 3),
                Direction = Direction.East,
                ExitPoint = new Tuple<ushort, ushort>(2, 2),
                Mines = new[] { new Tuple<ushort, ushort>(0, 1), new Tuple<ushort, ushort>(1, 0), new Tuple<ushort, ushort>(1, 2), new Tuple<ushort, ushort>(2, 1) },
                StartPoint = new Tuple<ushort, ushort>(0, 0)
            };

            var game = new Game(settings);
            game.OnMineHit += (sender, e) => { mineHit++; };
            game.OnStillInDanger += (sender, e) => { stillInDanger++; };
            game.OnSuccess += (sender, e) => { success++; };

            string moves = "M";

            //act
            game.Play(moves);

            //assert
            Assert.Equal(1, mineHit);
            Assert.Equal(0, stillInDanger);
            Assert.Equal(0, success);
            Assert.True(new Tuple<ushort, ushort>(1, 0).Equals(game.CurrentPosition));
        }

        [Fact]
        public void WhenPlay10_ThenMineHit()
        {
            //arrange
            int mineHit = 0;
            int stillInDanger = 0;
            int success = 0;

            GameSettings settings = new GameSettings
            {
                BoardSize = new Tuple<ushort, ushort>(3, 3),
                Direction = Direction.East,
                ExitPoint = new Tuple<ushort, ushort>(2, 2),
                Mines = new[] { new Tuple<ushort, ushort>(0, 1), new Tuple<ushort, ushort>(1, 0), new Tuple<ushort, ushort>(1, 2), new Tuple<ushort, ushort>(2, 1) },
                StartPoint = new Tuple<ushort, ushort>(0, 0)
            };

            var game = new Game(settings);
            game.OnMineHit += (sender, e) => { mineHit++; };
            game.OnStillInDanger += (sender, e) => { stillInDanger++; };
            game.OnSuccess += (sender, e) => { success++; };

            string moves = "RM";

            //act
            game.Play(moves);

            //assert
            Assert.Equal(1, mineHit);
            Assert.Equal(0, stillInDanger);
            Assert.Equal(0, success);
            Assert.True(new Tuple<ushort, ushort>(0, 1).Equals(game.CurrentPosition));
        }

        [Fact]
        public void WhenPlay11_ThenMineHit()
        {
            //arrange
            int mineHit = 0;
            int stillInDanger = 0;
            int success = 0;

            GameSettings settings = new GameSettings
            {
                BoardSize = new Tuple<ushort, ushort>(3, 3),
                Direction = Direction.South,
                ExitPoint = new Tuple<ushort, ushort>(0, 2),
                Mines = new[] { new Tuple<ushort, ushort>(1, 1), new Tuple<ushort, ushort>(1, 0) },
                StartPoint = new Tuple<ushort, ushort>(0, 0)
            };

            var game = new Game(settings);
            game.OnMineHit += (sender, e) => { mineHit++; };
            game.OnStillInDanger += (sender, e) => { stillInDanger++; };
            game.OnSuccess += (sender, e) => { success++; };

            string moves = "MRRMRM";

            //act
            game.Play(moves);

            //assert
            Assert.Equal(1, mineHit);
            Assert.Equal(0, stillInDanger);
            Assert.Equal(0, success);
            Assert.True(new Tuple<ushort, ushort>(1, 0).Equals(game.CurrentPosition));
        }

        [Fact]
        public void WhenPlay12_ThenMineHit()
        {
            //arrange
            int mineHit = 0;
            int stillInDanger = 0;
            int success = 0;

            GameSettings settings = new GameSettings
            {
                BoardSize = new Tuple<ushort, ushort>(3, 3),
                Direction = Direction.South,
                ExitPoint = new Tuple<ushort, ushort>(0, 2),
                Mines = new[] { new Tuple<ushort, ushort>(1, 1), new Tuple<ushort, ushort>(1, 0) },
                StartPoint = new Tuple<ushort, ushort>(0, 0)
            };

            var game = new Game(settings);
            game.OnMineHit += (sender, e) => { mineHit++; };
            game.OnStillInDanger += (sender, e) => { stillInDanger++; };
            game.OnSuccess += (sender, e) => { success++; };

            string moves = "MRRMRRMRRRM";

            //act
            game.Play(moves);

            //assert
            Assert.Equal(1, mineHit);
            Assert.Equal(0, stillInDanger);
            Assert.Equal(0, success);
            Assert.True(new Tuple<ushort, ushort>(1, 1).Equals(game.CurrentPosition));
        }

        [Fact]
        public void WhenPlay13_ThenMineHit()
        {
            //arrange
            int mineHit = 0;
            int stillInDanger = 0;
            int success = 0;

            GameSettings settings = new GameSettings
            {
                BoardSize = new Tuple<ushort, ushort>(3, 3),
                Direction = Direction.South,
                ExitPoint = new Tuple<ushort, ushort>(0, 2),
                Mines = new[] { new Tuple<ushort, ushort>(1, 1), new Tuple<ushort, ushort>(1, 0) },
                StartPoint = new Tuple<ushort, ushort>(0, 0)
            };

            var game = new Game(settings);
            game.OnMineHit += (sender, e) => { mineHit++; };
            game.OnStillInDanger += (sender, e) => { stillInDanger++; };
            game.OnSuccess += (sender, e) => { success++; };

            string moves = "MRRMRRMRRRM";

            //act
            game.Play(moves);

            //assert
            Assert.Equal(1, mineHit);
            Assert.Equal(0, stillInDanger);
            Assert.Equal(0, success);
            Assert.True(new Tuple<ushort, ushort>(1, 1).Equals(game.CurrentPosition));
        }
    }
}
