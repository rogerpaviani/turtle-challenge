﻿using System;
using System.IO;
using System.Reflection;

namespace Challenge.Tests.Unit
{
    internal class ResourceHelper
    {
        public static string GetStringData(string name)
        {
            var assembly = Assembly.GetExecutingAssembly();

            using (var stream = assembly.GetManifestResourceStream($"{assembly.GetName().Name}.TestFiles.{name}"))
            {
                if (stream == null)
                {
                    throw new ArgumentException();
                }

                using (var reader = new StreamReader(stream))
                {
                    return reader.ReadToEnd();
                }
            }
        }
    }
}
