﻿using Challenge.Settings;
using System;
using System.IO;
using Xunit;

namespace Challenge.Tests.Unit.SettingsTests
{
    public class LoadSettingsUnitTests
    {
        [Fact]
        public void WhenLoadingConfig1_ShouldReturnResult()
        {
            using (var stream = new MemoryStream())
            {
                //arrange
                byte[] bytes = System.Text.Encoding.UTF8.GetBytes(ResourceHelper.GetStringData("config1.txt"));
                stream.Write(bytes, 0, bytes.Length);

                //act
                var settings = LoadSettings.Load(stream);

                //assert
                Assert.NotNull(settings);
                Assert.True(new Tuple<ushort, ushort>(2, 2).Equals(settings.BoardSize));
                Assert.True(new Tuple<ushort, ushort>(0, 0).Equals(settings.StartPoint));
                Assert.True(new Tuple<ushort, ushort>(0, 1).Equals(settings.ExitPoint));
                Assert.Empty(settings.Mines);
                Assert.Equal(Direction.South, settings.Direction);
            }
        }

        [Fact]
        public void WhenLoadingConfig2_ShouldReturnResult()
        {
            using (var stream = new MemoryStream())
            {
                //arrange
                byte[] bytes = System.Text.Encoding.UTF8.GetBytes(ResourceHelper.GetStringData("config2.txt"));
                stream.Write(bytes, 0, bytes.Length);

                //act
                var settings = LoadSettings.Load(stream);

                //assert
                Assert.NotNull(settings);
                Assert.True(new Tuple<ushort, ushort>(2, 2).Equals(settings.BoardSize));
                Assert.True(new Tuple<ushort, ushort>(0, 0).Equals(settings.StartPoint));
                Assert.Null(settings.ExitPoint);
                Assert.Empty(settings.Mines);
                Assert.Equal(Direction.East, settings.Direction);
            }
        }

        [Fact]
        public void WhenLoadingConfig3_ShouldReturnResult()
        {
            using (var stream = new MemoryStream())
            {
                //arrange
                byte[] bytes = System.Text.Encoding.UTF8.GetBytes(ResourceHelper.GetStringData("config3.txt"));
                stream.Write(bytes, 0, bytes.Length);

                //act
                var settings = LoadSettings.Load(stream);

                //assert
                Assert.NotNull(settings);
                Assert.True(new Tuple<ushort, ushort>(3, 3).Equals(settings.BoardSize));
                Assert.Null(settings.StartPoint);
                Assert.True(new Tuple<ushort, ushort>(1, 1).Equals(settings.ExitPoint));
                Assert.NotEmpty(settings.Mines);
                Assert.Equal(Direction.East, settings.Direction);
            }
        }

        [Fact]
        public void WhenLoadingConfig4_ShouldReturnResult()
        {
            using (var stream = new MemoryStream())
            {
                //arrange
                byte[] bytes = System.Text.Encoding.UTF8.GetBytes(ResourceHelper.GetStringData("config4.txt"));
                stream.Write(bytes, 0, bytes.Length);

                //act
                var settings = LoadSettings.Load(stream);

                //assert
                Assert.NotNull(settings);
                Assert.True(new Tuple<ushort, ushort>(2, 2).Equals(settings.BoardSize));
                Assert.True(new Tuple<ushort, ushort>(0, 0).Equals(settings.StartPoint));
                Assert.True(new Tuple<ushort, ushort>(0, 1).Equals(settings.ExitPoint));
                Assert.Null(settings.Mines);
                Assert.Equal(Direction.South, settings.Direction);
            }
        }
    }
}
