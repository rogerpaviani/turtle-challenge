﻿using Challenge.Movement;
using System.IO;
using Xunit;

namespace Challenge.Tests.Unit.MovementTests
{
    public class LoadMovementsUnitTests
    {
        [Fact]
        public void WhenLoadingMovements1_ShouldReturnResult()
        {
            using (var stream = new MemoryStream())
            {
                //arrange
                byte[] bytes = System.Text.Encoding.UTF8.GetBytes(ResourceHelper.GetStringData("movements1.txt"));
                stream.Write(bytes, 0, bytes.Length);

                //act
                var sequences = LoadMovements.Load(stream);

                //assert
                Assert.NotEmpty(sequences);
                Assert.Equal(3, sequences.Length);
                Assert.Equal("rrrm", sequences[2]);
            }
        }

        [Fact]
        public void WhenLoadingMovements2_ShouldReturnResult()
        {
            using (var stream = new MemoryStream())
            {
                //arrange
                byte[] bytes = System.Text.Encoding.UTF8.GetBytes(ResourceHelper.GetStringData("movements2.txt"));
                stream.Write(bytes, 0, bytes.Length);

                //act
                var sequences = LoadMovements.Load(stream);

                //assert
                Assert.NotEmpty(sequences);
                Assert.Equal(14, sequences.Length);
                Assert.Equal("m", sequences[2]);
                Assert.Equal("mmrm", sequences[13]);
            }
        }
    }
}
