using System;
using Challenge.Movement;
using Xunit;

namespace Challenge.Tests.Unit.MovementTests
{
    public class SouthboundUnitTests
    {
        [Fact]
        public void WhenRotateRight_ShouldReturnWestbound()
        {
            //arrange
            var direction = Southbound.Instance;

            //act
            var newDirection = direction.RotateRight();

            //assert
            Assert.IsType<Westbound>(newDirection);
        }

        [Fact]
        public void WhenMoveForward_ShouldMoveDown()
        {
            //arrange
            var boardSize = new Tuple<ushort, ushort>(10, 10);
            var position = new Tuple<ushort, ushort>(4, 4);
            var expected = new Tuple<ushort, ushort>(4, 5);
            var direction = Southbound.Instance;

            //act
            var newPosition = direction.MoveForward(position, boardSize);

            //assert
            Assert.Equal(expected, newPosition);
        }
    }
}
