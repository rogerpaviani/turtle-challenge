using System;
using Challenge.Movement;
using Xunit;

namespace Challenge.Tests.Unit.MovementTests
{
    public class WestboundUnitTests
    {
        [Fact]
        public void WhenRotateRight_ShouldReturnNorthbound()
        {
            //arrange
            var direction = Westbound.Instance;

            //act
            var newDirection = direction.RotateRight();

            //assert
            Assert.IsType<Northbound>(newDirection);
        }

        [Fact]
        public void WhenMoveForward_ShouldMoveRight()
        {
            //arrange
            var boardSize = new Tuple<ushort, ushort>(10, 10);
            var position = new Tuple<ushort, ushort>(4, 4);
            var expected = new Tuple<ushort, ushort>(3, 4);
            var direction = Westbound.Instance;

            //act
            var newPosition = direction.MoveForward(position, boardSize);

            //assert
            Assert.Equal(expected, newPosition);
        }
    }
}
