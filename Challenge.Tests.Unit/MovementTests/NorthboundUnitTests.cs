using Challenge.Movement;
using System;
using Xunit;

namespace Challenge.Tests.Unit.MovementTests
{
    public class NorthboundUnitTests
    {
        [Fact]
        public void WhenRotateRight_ShouldReturnEastbound()
        {
            //arrange
            var direction = Northbound.Instance;

            //act
            var newDirection = direction.RotateRight();

            //assert
            Assert.IsType<Eastbound>(newDirection);
        }

        [Fact]
        public void WhenMoveForward_ShouldMoveRight()
        {
            //arrange
            var boardSize = new Tuple<ushort, ushort>(10, 10);
            var position = new Tuple<ushort, ushort>(4, 4);
            var expected = new Tuple<ushort, ushort>(4, 3);
            var direction = Northbound.Instance;

            //act
            var newPosition = direction.MoveForward(position, boardSize);

            //assert
            Assert.Equal(expected, newPosition);
        }
    }
}
