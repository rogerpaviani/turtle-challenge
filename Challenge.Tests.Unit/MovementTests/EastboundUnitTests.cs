using System;
using Challenge.Movement;
using Xunit;

namespace Challenge.Tests.Unit.MovementTests
{
    public class EastboundUnitTests
    {
        [Fact]
        public void WhenRotateRight_ShouldReturnSouthbound()
        {
            //arrange
            var direction = Eastbound.Instance;

            //act
            var newDirection = direction.RotateRight();

            //assert
            Assert.IsType<Southbound>(newDirection);
        }

        [Fact]
        public void WhenMoveForward_ShouldMoveLeft()
        {
            //arrange
            var boardSize = new Tuple<ushort, ushort>(10, 10);
            var position = new Tuple<ushort, ushort>(4, 4);
            var expected = new Tuple<ushort, ushort>(5, 4);
            var direction = Eastbound.Instance;

            //act
            var newPosition = direction.MoveForward(position, boardSize);

            //assert
            Assert.Equal(expected, newPosition);
        }
    }
}
