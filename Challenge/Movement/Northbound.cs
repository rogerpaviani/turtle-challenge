﻿using System;

namespace Challenge.Movement
{
    internal class Northbound : IMovement
    {
        private static Northbound _instance;
        public static Northbound Instance
        {
            get
            {
                return _instance = new Northbound();
            }
        }

        private Northbound()
        {
            //
        }

        public IMovement RotateRight()
        {
            return Eastbound.Instance;
        }

        public Tuple<ushort, ushort> MoveForward(Tuple<ushort, ushort> currentPosition, Tuple<ushort, ushort> boardSize)
        {
            return new Tuple<ushort, ushort>(currentPosition.Item1, (ushort)Math.Max(currentPosition.Item2 - 1, 0));
        }
    }
}
