﻿using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Challenge.Movement
{
    internal class LoadMovements
    {
        public static string[] Load(Stream stream)
        {
            var sequences = new List<string>();

            stream.Seek(0, SeekOrigin.Begin);
            using (StreamReader streamReader = new StreamReader(stream, Encoding.UTF8))
            {
                string line;
                while ((line = streamReader.ReadLine()) != null)
                {
                    sequences.Add(line);
                }
            }

            return sequences.ToArray();
        }
    }
}
