﻿using System;

namespace Challenge.Movement
{
    internal class Southbound : IMovement
    {
        private static Southbound _instance;
        public static Southbound Instance
        {
            get
            {
                return _instance = new Southbound();
            }
        }

        private Southbound()
        {
            //
        }

        public IMovement RotateRight()
        {
            return Westbound.Instance;
        }

        public Tuple<ushort, ushort> MoveForward(Tuple<ushort, ushort> currentPosition, Tuple<ushort, ushort> boardSize)
        {
            return new Tuple<ushort, ushort>(currentPosition.Item1, (ushort)Math.Min(currentPosition.Item2 + 1, boardSize.Item2 - 1));
        }
    }
}
