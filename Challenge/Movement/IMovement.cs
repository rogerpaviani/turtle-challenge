﻿using System;

namespace Challenge.Movement
{
    internal interface IMovement
    {
        IMovement RotateRight();
        Tuple<ushort, ushort> MoveForward(Tuple<ushort, ushort> currentPosition, Tuple<ushort, ushort> boardSize);
    }
}
