﻿using System;

namespace Challenge.Movement
{
    internal class Westbound : IMovement
    {
        private static Westbound _instance;
        public static Westbound Instance
        {
            get
            {
                return _instance = new Westbound();
            }
        }

        private Westbound()
        {
            //
        }

        public IMovement RotateRight()
        {
            return Northbound.Instance;
        }

        public Tuple<ushort, ushort> MoveForward(Tuple<ushort, ushort> currentPosition, Tuple<ushort, ushort> boardSize)
        {
            return new Tuple<ushort, ushort>((ushort)Math.Max(currentPosition.Item1 - 1, 0), currentPosition.Item2);
        }
    }
}
