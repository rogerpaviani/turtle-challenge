﻿using System;

namespace Challenge.Movement
{
    internal class Eastbound : IMovement
    {
        private static Eastbound _instance;
        public static Eastbound Instance
        {
            get
            {
                return _instance = new Eastbound();
            }
        }

        private Eastbound()
        {
            //
        }

        public IMovement RotateRight()
        {
            return Southbound.Instance;
        }

        public Tuple<ushort, ushort> MoveForward(Tuple<ushort, ushort> currentPosition, Tuple<ushort, ushort> boardSize)
        {
            return new Tuple<ushort, ushort>((ushort)Math.Min(currentPosition.Item1 + 1, boardSize.Item1 - 1), currentPosition.Item2);
        }
    }
}
