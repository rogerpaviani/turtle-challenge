﻿using System;

namespace Challenge.Settings
{
    class GameSettings
    {
        public Tuple<ushort, ushort> BoardSize;
        public Direction Direction;
        public Tuple<ushort, ushort> ExitPoint;
        public Tuple<ushort, ushort>[] Mines;
        public Tuple<ushort, ushort> StartPoint;
    }
}
