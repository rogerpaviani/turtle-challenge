﻿using System;
using System.IO;
using System.Linq;
using System.Text;

namespace Challenge.Settings
{
    internal class LoadSettings
    {
        public static GameSettings Load(Stream stream)
        {
            var settings = new GameSettings();

            stream.Seek(0, SeekOrigin.Begin);
            using (StreamReader streamReader = new StreamReader(stream, Encoding.UTF8))
            {
                string line;
                while ((line = streamReader.ReadLine()) != null)
                {
                    MapLine(settings, line);
                }
            }

            return settings;
        }

        private static void MapLine(GameSettings settings, string line)
        {
            var tokens = line.Split('=', StringSplitOptions.RemoveEmptyEntries);
            if (tokens.Length != 2) return;

            switch (tokens[0].ToLower())
            {
                case "boardsize":
                    settings.BoardSize = MapTuple(tokens[1]);
                    break;
                case "exitpoint":
                    settings.ExitPoint = MapTuple(tokens[1]);
                    break;
                case "startpoint":
                    settings.StartPoint = MapTuple(tokens[1]);
                    break;
                case "mines":
                    settings.Mines = tokens[1].Split(';', StringSplitOptions.RemoveEmptyEntries).Select(MapTuple).ToArray();
                    break;
                case "direction":
                    settings.Direction = MapDirection(tokens[1]);
                    break;
            }
        }

        private static Direction MapDirection(string direction)
        {
            return Enum.Parse<Direction>(direction, true);
        }

        private static Tuple<ushort, ushort> MapTuple(string tuple)
        {
            var tokens = tuple.Split(',', StringSplitOptions.RemoveEmptyEntries);
            if (tokens.Length != 2) return null;

            return new Tuple<ushort, ushort>(ushort.Parse(tokens[0]), ushort.Parse(tokens[1]));
        }
    }
}
