﻿using Challenge.Movement;
using Challenge.Settings;
using System;
using System.Linq;

namespace Challenge
{
    class Game
    {
        private readonly GameSettings _settings;

        public event EventHandler OnMineHit;
        public event EventHandler OnSuccess;
        public event EventHandler OnStillInDanger;

        public Tuple<ushort, ushort> CurrentPosition { get; private set; }
        private IMovement _currentMovement;

        public Game(GameSettings settings)
        {
            CheckNullSettings(settings);
            CheckValidPositions(settings);

            _settings = settings;
            Reset();
        }

        public void Play(string moves)
        {
            moves = moves.ToLower();
            for (var i = 0; i < moves.Length; i++)
            {
                if (moves[i] == 'r')
                {
                    _currentMovement = _currentMovement.RotateRight();
                }
                else if (moves[i] == 'm')
                {
                    CurrentPosition = _currentMovement.MoveForward(CurrentPosition, _settings.BoardSize);
                    if (CurrentPosition.Equals(_settings.ExitPoint))
                    {
                        RaiseSuccess();
                        return;
                    }
                    if (_settings.Mines.Any(a => a.Equals(CurrentPosition)))
                    {
                        RaiseMineHit();
                        return;
                    }
                }
            }

            RaiseStillInDanger();
        }

        private void RaiseMineHit()
        {
            OnMineHit?.Invoke(this, null);
        }

        private void RaiseSuccess()
        {
            OnSuccess?.Invoke(this, null);
        }

        private void RaiseStillInDanger()
        {
            OnStillInDanger?.Invoke(this, null);
        }

        private void CheckValidPositions(GameSettings settings)
        {
            if (settings.BoardSize.Item1 < settings.ExitPoint.Item1 || settings.BoardSize.Item2 < settings.ExitPoint.Item2)
            {
                throw new ArgumentOutOfRangeException(nameof(settings.ExitPoint), "Invalid position");
            }

            if (settings.BoardSize.Item1 < settings.StartPoint.Item1 || settings.BoardSize.Item2 < settings.StartPoint.Item2)
            {
                throw new ArgumentOutOfRangeException(nameof(settings.StartPoint), "Invalid position");
            }

            if (settings.ExitPoint.Equals(settings.StartPoint))
            {
                throw new ArgumentException("Start point and Exit point must be in different positions");
            }

            for (int i = 0; i < settings.Mines.Length; i++)
            {
                if (settings.BoardSize.Item1 < settings.Mines[i].Item1 || settings.BoardSize.Item2 < settings.Mines[i].Item2)
                {
                    throw new ArgumentOutOfRangeException(nameof(settings.Mines), $"Mine #{i} is in an invalid position");
                }

                if (settings.ExitPoint.Equals(settings.Mines[i]))
                {
                    throw new ArgumentException($"Mine #{i} is in an invalid position (same as Exit point)");
                }

                if (settings.StartPoint.Equals(settings.Mines[i]))
                {
                    throw new ArgumentException($"Mine #{i} is in an invalid position (same as Start point)");
                }
            }
        }

        private void CheckNullSettings(GameSettings settings)
        {
            if (settings == null)
            {
                throw new ArgumentNullException(nameof(settings), "Null settings");
            }

            if (settings.BoardSize == null)
            {
                throw new ArgumentNullException(nameof(settings), "Missing board size");
            }

            if (settings.ExitPoint == null)
            {
                throw new ArgumentNullException(nameof(settings), "Missing exiting point");
            }

            if (settings.Mines == null)
            {
                throw new ArgumentNullException(nameof(settings), "Missing mines");
            }

            if (settings.StartPoint == null)
            {
                throw new ArgumentNullException(nameof(settings), "Missing starting point");
            }
        }

        public void Reset()
        {
            CurrentPosition = new Tuple<ushort, ushort>(_settings.StartPoint.Item1, _settings.StartPoint.Item2);

            switch (_settings.Direction)
            {
                case Direction.North:
                    _currentMovement = Northbound.Instance;
                    break;
                case Direction.East:
                    _currentMovement = Eastbound.Instance;
                    break;
                case Direction.South:
                    _currentMovement = Southbound.Instance;
                    break;
                case Direction.West:
                    _currentMovement = Westbound.Instance;
                    break;
            }
        }
    }
}
