﻿using Challenge.Movement;
using Challenge.Settings;
using System;
using System.IO;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("Challenge.Tests.Unit")]
namespace Challenge
{
    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
    class Program
    {
        static int Main(string[] args)
        {
            if (args == null || args.Length != 2)
            {
                Console.WriteLine("Invalid args. Please specify 'settingsFile' and 'movementsFile'.");
                return 1;
            }

            var settingsFile = args[0];
            var movementsFile = args[1];

            GameSettings initialSettings = LoadSettingsFromFile(settingsFile);
            var game = new Game(initialSettings);

            var currentGame = 0;
            game.OnMineHit += (obj, result) =>
            {
                Console.WriteLine($"Sequence {currentGame}: Mine hit!");
            };
            game.OnStillInDanger += (obj, result) =>
            {
                Console.WriteLine($"Sequence {currentGame}: Still in danger!");
            };
            game.OnSuccess += (obj, result) =>
            {
                Console.WriteLine($"Sequence {currentGame}: Success!");
            };

            string[] sequences = LoadMovementsFromFile(movementsFile);
            for (var i = 0; i < sequences.Length; i++)
            {
                currentGame = i;
                game.Play(sequences[i]);
                game.Reset();
            }

            return 0;
        }

        private static string[] LoadMovementsFromFile(string movementsFile)
        {
            using (var fileStream = File.OpenRead(movementsFile))
            {
                return LoadMovements.Load(fileStream);
            }
        }

        private static GameSettings LoadSettingsFromFile(string fileName)
        {
            using (var fileStream = File.OpenRead(fileName))
            {
                return LoadSettings.Load(fileStream);
            }
        }
    }
}
