# turtle-challenge

## Sample 'settings' file contents:

<code>boardsize=5,5
startpoint=1,3
exitpoint=3,4
mines=1,0;3,0;1,2;2,2;3,2;2,3;2,4;4,4;
direction=west
</code>

## Sample 'movements' file contents:

<code>mrmmrmmmmrmmrmrm
mrmmrmmmmrmmrmrrm
mrmmrmmmmrmmrmrrrm
</code>